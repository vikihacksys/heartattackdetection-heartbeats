const mongoose = require('mongoose')

const heartbeatSchema = mongoose.Schema({
	_id: mongoose.Schema.Types.ObjectId,
	patient_id: { type: String, required: true },
	bpm: { type: String, required: true},
	date: { type: Date, required: true}
})

module.exports = mongoose.model('Heartbeat', heartbeatSchema)