const express = require('express');
const router = express.Router();
const mongoose = require('mongoose')
const request = require("request");

const Heartbeat = require('../models/heartbeatModel')


/* 

	API: "http://localhost:8081/heartbeat/addHeartbeat"

	Parameters: 
		patient_id: dddasdjasdn5adas
		bpm: 75
		date: 12/03/2020 12:00:00

	HTTP-method: POST	

*/

router.post('/addHeartbeat', (req, res, next) => {

    let heartbeat = new Heartbeat ({
		_id: new mongoose.Types.ObjectId(),
		patient_id: req.body.patient_id,
		bpm: req.body.bpm,
		date: req.body.date   
	}) 

	heartbeat.save()
	.then(result => { 
 
		res.status(200).json({
			status: true,
			message: "Heartbeat added successfully.", 
			data: heartbeat
		})
 
	})
	.catch(error => {
		sendErrorResponse(res, error)
	})

})



/* 

	API: "http://localhost:8081/heartbeat/getHeartbeatsList"

	Parameters: 
		patient_id: sdfjnsdfa555eaafaf

	HTTP-method: POST	

*/

router.post('/getHeartbeatsList', (req, res, next) => {

	Heartbeat.find({'patient_id': req.body.patient_id})
    	.exec()
    	.then(result => {
  
    		if (result.length > 0) { 
   
    			var resultsCount = result.length 
    			var lowBPM = result[0]["bpm"]
    			var highBPM = lowBPM
    			var averageBPM = 0
    			var sumOfBPM = 0 

    			for (var i = 0; i < result.length; i++) {

    			 	var bpmValue = parseFloat(result[i]["bpm"])
    			 	sumOfBPM += bpmValue 

    			 	if (bpmValue <= lowBPM) {
    			 		lowBPM = bpmValue
    			 	}

    			 	if (bpmValue >= highBPM) {
    			 		highBPM = bpmValue
    			 	}

    		    }
 
    		    averageBPM = sumOfBPM / resultsCount

    		    result.sort(function(a, b) {
				    var c = new Date(a.date);
				    var d = new Date(b.date);
				    return d-c;
				})

    			res.status(200).json({
					status: true,
					message: "", 
					data: {
						"heartbeats": result,
						"lowBPM": lowBPM,
						"highBPM": highBPM,
						"averageBPM": averageBPM 
					}
				})
 
    		} else {
 
			     res.status(500).json({
					status: false,
					message: "No heartbeats available.",
					data: {} 
			    })

    		}
    		
    	})
    	.catch(error => { 
    		sendErrorResponse(res, error)	
    	})
 
})
 

function sendErrorResponse (res, error) {
	res.status(500).json({
		status: false,
		message: "",
		data: error
    })
} 

module.exports = router